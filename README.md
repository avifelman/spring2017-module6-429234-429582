# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for a chatroom where users can create private and public rooms with arbitrary names. Users can join other rooms and move back and forth between public rooms and password protected private rooms. Each room has an owner.

Users have a username and can see who is online in the room they are in. They can also directly message private messages that appear in another color to other users in that room, and invite other users to that room with an alert message that you have been invited.You can scroll through old messages in chat rooms. 

### How do I get set up? ###

http://ec2-54-191-6-127.us-west-2.compute.amazonaws.com:3456/

Once node client-server.js is running, you can load this link and view client.html stylized by client.css


### Who do I talk to? ###

Avi Felman and Casey Sands
Cjsands@wustl.edu
Avi.felmans@gmail.com