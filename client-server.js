var http = require("http"),
        socketio = require("socket.io"),
        fs = require("fs");

var app = http.createServer(function(req, resp){

        fs.readFile("client.html", function(err, data){

        if(err) return resp.writeHead(500);
                resp.writeHead(200);
                resp.end(data);
        });
});
app.listen(3456);

var io = socketio.listen(app);
usernameList = {"default":new Array()};
messages = [];
rooms = {"default":new Array()};
roomOwner = {"default":" "};
roomPassword = {};
privateRooms={};

io.sockets.on("connection", function(socket){
          socket.on("join", function(data){
                                socket.username = data.message;
                                username = socket.username;
                                socket.room = "default";
                                (usernameList[socket.room]).push(username);
                                console.log("message: "+socket.username);
                                io.sockets.emit("disp_nickname", {message:socket.username});
                                io.sockets.emit("disp_nickname", {message:socket.username});
                        /*      for(var x=0;x<rooms.length;x++){
                                        console.log((Object.keys(rooms))[x]);
                                         io.sockets.emit("room_options",{message:(Object.keys(rooms))[x]});
                                }*/
                                io.sockets.emit("room_options",{message:(Object.keys(rooms))});
                                io.sockets.emit("private_room_options",{message:Object.keys(privateRooms)});
                });
                socket.on("make_new_room",function(data){
                        var newRoom = data.message;
                        rooms[newRoom]=new Array();
                        usernameList[newRoom]=new Array();
                        roomOwner[newRoom]=socket.username;
                        //roomPassword[newRoom]=data.message2;
                        io.sockets.emit("room_options",{message:Object.keys(rooms)});

                });

                 socket.on("make_priv_room",function(data){
                        var string = data.message;
                         var str = string.split(",");
                        var newRoom = str[0];
                        privateRooms[newRoom]=new Array();
                        usernameList[newRoom]=new Array();
                        roomOwner[newRoom]=socket.username;
                        roomPassword[newRoom]=str[1];
                        io.sockets.emit("private_room_options",{message:Object.keys(privateRooms)});
                        //console.log(roomOwner[newRoom] + newRoom);
                        //print(Object.keys(room).toString());
                                                                });

                socket.on("join", function(data){
                        var roomMsg = rooms[socket.room];
                        var roomUsers = usernameList[socket.room];
                        var txt = "";
						var x;
						var check = [];
                        for (x in roomUsers) {
                            check.push(x);
							if(~(check.indexOf(x) > -1)){
								txt += roomUsers[x] + ",";
							}
                        }
                        var txt2 = "";
                        var x2;
                        for (x2 in roomMsg) {
                                txt2 += roomMsg[x2] + ",";
                        }
                        console.log("message: "+ txt);
                        io.sockets.emit("users_on", {message:txt});
                        io.sockets.emit("message_to_client", {message:txt2});
                        //rooms[socket.room]=messages;
                        usernameList[socket.room]=roomUsers;
                        rooms[socket.room] = roomMsg;
                });


                socket.on("joinRoom",function(data){
                        console.log("message: "+socket.username);
                        socket.room = data.message;
						userRoom = usernameList[socket.room];
						userRoom.push(socket.username);
						usernameList[socket.room]=userRoom;
                        /*if(roomOwner[socket.room] == socket.username){
                                io.sockets.connected[socket.id].emit("kick_out",{message:usernameList[socket.room]});
                        }
                        */
                        io.sockets.emit("disp_nickname", {message:socket.username});
                        //console.log(roomOwner[socket.room]+socket.room);
                });
                socket.on("joinRoom",function(data){
                        //change innerHTML of chatlog
                        //makemultiple message threads, use dictionary
                        var roomMsg = rooms[socket.room];
                        var roomUsers = usernameList[socket.room];
						var check = [];
                        var txt = "";
                        var x;
                        for (x in roomUsers) {
							check.push(x);
							if(~(check.indexOf(x) > -1)){
								txt += roomUsers[x] + ",";
							}
							else{
								console.log("Duplicate: "+ check.indexOf(x));
							}
                        }
                        var txt2 = "";
                        var x2;
                        for (x2 in roomMsg) {
                                txt2 += roomMsg[x2] + ",";
                        }
                        console.log("message: "+ txt);
						console.log("message: "+ check);
                        io.sockets.emit("users_on", {message:txt});
                        io.sockets.emit("message_to_client", {message:txt2});
                        rooms[socket.room]=roomMsg;
                        usernameList[socket.room]=roomUsers;
                });
                socket.on("checkPassword", function(data){
                        var password_try = data.message;
                        //console.log("passtry "+password_try);
                        //console.log(data.message2);
                        if(password_try == roomPassword[data.message2]){
                                io.sockets.emit("joinSuccess",{message:data.message2});
                        }
                        else{
                                io.sockets.emit("joinFail",{message:data.message2});
}
                });
                //check password first!
                socket.on("joinPrivRoom",function(data){
                        console.log("message: "+socket.username);
                        socket.room = data.message;
                        userRoom = usernameList[socket.room];
                        userRoom.push(socket.username);
                        io.sockets.emit("disp_nickname", {message:socket.username});

                });
                 socket.on("joinPrivRoom",function(data){
                        var roomMsg = privateRooms[socket.room];
                        var roomUsers = usernameList[socket.room];
                        var txt = "";
                        var x;
                        for (x in roomUsers) {
                                txt += roomUsers[x] + ",";
                        }
                        var txt2 = "";
                        var x2;
                        for (x2 in roomMsg) {
                                txt2 += roomMsg[x2] + ",";
                        }
                        console.log("message: "+ txt);
                        io.sockets.emit("users_on", {message:txt});
                        io.sockets.emit("message_to_client", {message:txt2});
                        privateRooms[socket.room]=roomMsg;
                        usernameList[socket.room]=roomUsers;
                });

                socket.on('message_to_server', function(data) {
                        console.log("message: "+data.message); // log it to the Node.JS output
                        message = socket.username + ": " + data.message;
                        if(socket.room in rooms){
                                 var roomMes = rooms[socket.room];
                        }
                        else{
                                var roomMes = privateRooms[socket.room];
                        }
                        (roomMes).push(message);
                         if(socket.room in rooms){
                                rooms[socket.room]=roomMes;
                                var roomMsg = rooms[socket.room];
                        }
                        else{
                                privateRooms[socket.room]=roomMes;
                                var roomMsg = privateRooms[socket.room];
                        }
                        //var roomMsg = rooms[socket.room];
                        var txt2 = "";
                        var x2;
                        for (x2 in roomMsg) {
                                txt2 += roomMsg[x2] + ",";
                        }
                        if(socket.room in rooms){
                                rooms[socket.room]=roomMsg;
                        }
 else{
                                privateRooms[socket.room]=roomMsg;
                        }
                        io.sockets.emit("message_to_client", {message:txt2}); // broadcast the message to other users

                });
                socket.on("direct_message",function(data){
                        var msg = data.message;
                         var str = msg.split(",");
                        var target = str[1];
                        var message_content = str[0];
                console.log(target + "target");
                //      var clients = io.sockets.clients();
                        for (s in io.sockets.connected){
                                //var target_id = io.sockets.sockets[s].username
                                    //    console.log("user"+target_id);
                                if(io.sockets.sockets[s].username == target){
                                        var target_id = io.sockets.sockets[s].id
                                        console.log("user"+target_id+ " "+target);
                                        io.sockets.sockets[s].emit("direct_message_sent",{message:message_content+" from "+socket.username});
                                }
                        }
                });
				
				socket.on("invite_user", function(data){
                        var target_user = data.message;
                         for (s in io.sockets.connected){
                                 if(io.sockets.sockets[s].username == target_user){
                                         var target_id = io.sockets.sockets[s].id
                                         io.sockets.sockets[s].emit("invited",{message:socket.username+","+socket.room});
                                }
                        }
                });

});
